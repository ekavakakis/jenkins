package com.workshop.jenkins.springbootweb;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculatorController {
	
	@RequestMapping("/sum")
	public Double sum(@RequestParam(value="x") Double x, @RequestParam(value="y") Double y){
		return x + y;
	}
}
