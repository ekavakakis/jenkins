package com.workshop.jenkins.springbootweb;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

@SpringBootTest
public class CalculatorControllerTest {
	
	CalculatorController cntrl =  new CalculatorController();
	
	@Test
    public void exampleTest() {
		assert(this.cntrl.sum(5.0, 3.0)).equals(8.0);
    }
  }

